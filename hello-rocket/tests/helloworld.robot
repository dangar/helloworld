*** Settings ***
Library     Collections
Library     RequestsLibrary

***Test Cases***
Call Hello World API
    Create Session      rocket      http://localhost:8000
    ${resp}=    Get Request     rocket  /api/v1/helloworld
    Should Be Equal As Strings      ${resp.status_code}     200
    Log     ${resp}

Call Hello World 2 API
    Create Session      rocket      http://localhost:8000
    ${resp}=    Get Request     rocket  /api/v1/helloworld2
    Should Be Equal As Strings      ${resp.status_code}     200
    Log     ${resp}
