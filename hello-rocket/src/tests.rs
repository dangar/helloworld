
#[cfg(test)]
mod tests {
    use crate::rocket;
    use rocket::local::Client;
    use rocket::http::Status;

    #[test]
    fn hello_world() {
        let _client = Client::new(rocket()).expect("valid rocket instance");
        let mut response = _client.get("/api/v1/helloworld").dispatch();
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.body_string(), Some("Hello, world!".into()));
    }

    #[test]
    fn hello_world2() {
        let _client = Client::new(rocket()).expect("valid rocket instance");
        let mut response = _client.get("/api/v1/helloworld2").dispatch();
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.body_string(), Some("Hello, world 2!".into()));
    }
}
