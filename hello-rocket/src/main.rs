#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[cfg(test)] mod tests;

#[get("/helloworld")]
fn helloworld() -> &'static str {
    "Hello, world!"
}

#[get("/helloworld2")]
fn helloworld2() -> &'static str {
    "Hello, world 2!"
}

fn rocket() -> rocket::Rocket {
    rocket::ignite().mount("/api/v1/", routes![helloworld,helloworld2])
}

fn main() {
    rocket().launch();
}

